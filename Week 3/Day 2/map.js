const map = new Map([
    ["nama", "freeCodeCamp"],
    ["type", "blog"],
    ["writer", "Tapas Adhikary"],
  ]);

  const obj = {nama: "Tamam" , ttl: "12 September 1997", jk: "laki-laki"}

  console.log(map.get("nama"));
  console.log(map.get("type"));
  
  map.set("alamat", "jaksel");
  console.log(map.get ("alamat"))

  console.log(obj["nama"]);
  console.log(obj["ttl"]);