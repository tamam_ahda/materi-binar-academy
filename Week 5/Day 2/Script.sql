CREATE TABLE tbl_brg (
    kd_brg		int,
    nm_brg		varchar(100),
    harga_beli 	bigint,
    kd_pemasok 	int
);

CREATE TABLE tbl_pemasok (
    kd_pemasok	int,
    nm_pemasok 	varchar(100)
);

CREATE TABLE tbl_trans_jual (
    kd_brg 		int,
    kd_plg 		int,
    no_fak 		int,
	tgl_trans	date,
	qty			int,
	harga_jual	bigint
);

CREATE TABLE tbl_plg (
    kd_plg 	int,
    nm_plg 	varchar(100)
);

