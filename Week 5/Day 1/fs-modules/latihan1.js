const fs = require("fs");

// const handleRenameError = (err) => {
//     if (err) {
//         return console.error(err)
//     } else {
//         return console.log ("succes rename file")
// }

// fs.rename("before.json", "after.json", handleRenameError);

// atau

fs.rename("before.json", "after.json", (err) => {
    if (err) {
      return console.error(err)
    } else {
        return console.log ("succes rename file");
    }
  
  })