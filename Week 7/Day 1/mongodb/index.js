const mongoose = require("mongoose");

//Connect to mongodb server
(async () => {
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //init mongoose schema
  const Schema = mongoose.Schema;

  // Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  //Create Model Users
  const UsersModel = mongoose.model("users", Users);

  //Insert new Data
  // await UsersModel.create({ fullName: "John Mayer", age: 34 });

  // Get existing data
  //   const data = await UsersModel.find({});
  //   console.log(data);

  // Update data
  //   await UsersModel.updateOne(
  //     { _id: "600e874f5f73b95660ee1389" },
  //     { fullName: "Michael Jackson" }
  //   );
  //   const data = await UsersModel.find({});
  //   console.log(data);

  // Delete data
  await UsersModel.deleteOne({ _id: "600e874f5f73b95660ee1389" });
  const data = await UsersModel.find({});
  console.log(data);
})();


// Bikin di table users: 10 data users, datanya dibebaskan
// bikin CRUD post.js, create.js, put.js, delete.js.
