const mongoose = require("mongoose");

//Connect to mongodb server
(async () => {
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //init mongoose schema
  const Schema = mongoose.Schema;

  // Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  //Create Model Users
  const UsersModel = mongoose.model("users", Users);

  // Get existing data
  const data = await UsersModel.find({});
  console.log(data);
})();
