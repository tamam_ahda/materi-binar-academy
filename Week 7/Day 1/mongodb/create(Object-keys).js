const mongoose = require("mongoose");

//Connect to mongodb server
(async () => {
  await mongoose.connect("mongodb://localhost:27017/binaracademy", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true,
  });

  //init mongoose schema
  const Schema = mongoose.Schema;

  // Create Schema Users (Migrations)
  const Users = new Schema({
    fullName: String,
    age: Number,
  });

  //Create Model Users
  const UsersModel = mongoose.model("users", Users);

  //Insert new Data
  const dataInsert = [
    { fullName: "John Mayer", age: 34 },
    { fullName: "Bambang Pamungkas", age: 34 },
    { fullName: "Sulaiman", age: 33 },
    { fullName: "Samsul", age: 60 },
    { fullName: "Kadir", age: 51 },
    { fullName: "Suroso", age: 55 },
    { fullName: "Supardi", age: 54 },
    { fullName: "Yuda", age: 22 },
    { fullName: "Rizky", age: 22 },
    { fullName: "Madon", age: 22 },
  ];

  //Initiate allowed keys
  const allowedKeys = ["Name", "age"];

  try {
    //looping check
    for (let index = 0; index < dataInsert.length; index++) {
      const requestKeys = Object.keys(dataInsert[index]);

      for (let index2 = 0; index2 < requestKeys.length; index2++) {
        if (!allowedKeys.includes(requestKeys[index2])) {
          throw new Error(`data ${requestKeys[index2]} tidak sesuai format`);
        }
      }
    }

    await UsersModel.create(dataInsert);

    const data = await UsersModel.find({});
    console.log(data);
  } catch (error) {
    console.log("ada yang error");
  }
})();
