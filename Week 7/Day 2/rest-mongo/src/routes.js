const router = require("express").Router();
const usersModel = require("../models/users")();
const middlewareAddUsers = require("../middleware/addusers")
const middlewareEditUsers = require("../middleware/editusers")
const middlewareDeleteUsers = require("../middleware/delete")

module.exports = function routes() {
  router.get("/users", async (req, res) => {
    try {
      const data = await usersModel.find();
      res.json({ message: "succes read data users", data: data });
    } catch (error) {
      console.log(error)
      res.status(500).json("error when read data users");
    }
  });

  router.post("/users", middlewareAddUsers, async (req, res) => {
    try {
      await usersModel.create(req.body);
      res.json({ message: "success create new data users" });
    } catch (error) {
      console.log(error)
      res.status(500).json({ message: "error when create data users" });
    }
  });

  router.put("/users", middlewareEditUsers ,async (req, res) => {
    try {
      const id = req.body["id"];
      const fullName = req.body["fullName"];
      const age = req.body["age"]
      await usersModel.updateOne(
        { _id: id }, 
        { fullName: fullName, age: age}
      );

      res.json({ message: "success update data users" });
    } catch (error) {
      console.log(error)
      res.status(500).json({ message: "error when update data users" });
    }
  });

  router.delete("/users", middlewareDeleteUsers ,async (req, res) => {
    try {
      const id = req.query["id"]
      await usersModel.deleteOne({ _id: id });
      res.json({ message: "succes delete user data" });
    } catch (error) {
      console.log(error)
      res.status(500).json({ message: "error delete data users" });
    }
  });
  return router;
};
