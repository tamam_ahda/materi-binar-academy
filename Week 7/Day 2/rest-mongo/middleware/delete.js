module.exports = function deleteUsers(req, res, next) {
    const data = req.query["id"];
  
    try {
      // Empty request validation
      if (!data) {
        throw new Error("id user undifined");
      }
  
      next()
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  };
  