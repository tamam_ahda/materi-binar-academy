module.exports = function editUsers(req, res, next) {
    const allowedKeys = ["fullName", "age", "id"];
    const data = Object.keys(req.body);
  
    try {
      // Empty request validation
      if (!data.length) {
        throw new Error("request can't data empty");
      }
      if(data.length !== 3){
          throw new Error("data tidak sesuai format")
      }
  
      //
        for (let j = 0; j < data.length; j++) {
          if (!allowedKeys.includes(data[j])) {
            throw new Error("format request data is not valid");
          }
        }
  
      next()
    } catch (error) {
      res.status(400).json({ message: error.message });
    }
  };
  