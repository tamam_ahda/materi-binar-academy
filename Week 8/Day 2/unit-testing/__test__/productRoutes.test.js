const app = require("../app");

const request = require("supertest");

test("routes/product should return message this is products", () => {
  return request(app).get("/products").expect(200).expect("this is products");
});
