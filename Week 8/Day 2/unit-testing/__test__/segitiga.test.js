const luasSegitiga = require("../module/segitiga")

test("hasil fungsi luas segitiga harus sesuai dengan rumus luas segitiga", ()=> {
    const a = 10
    const t = 5
    const rumusLuasSegitiga = 0.5 * a* t
    expect(luasSegitiga(a,t)).toEqual(rumusLuasSegitiga)
})