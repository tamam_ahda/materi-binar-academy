const express = require("express");
const bodyParser = require("body-parser");
const multer = require("multer");
require("dotenv");

const port = process.env.NODE_PORT;

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

function renameFile(oldNames) {
  const [fileName, extensions] = oldNames.split(".");
  return `${fileName}-${Date.now()}.${extensions}`;
}

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "uploads");
  },
  filename: (req, file, cb) => {
    cb(null, renameFile(file.originalname));
  },
});

const upload = multer({ storage: storage });

app.post("/register", upload.single("profilePicture"), (req, res) => {
  res.send("succes upload");
});

app.listen(5000, () => console.log(`this app running on port 5000`));
