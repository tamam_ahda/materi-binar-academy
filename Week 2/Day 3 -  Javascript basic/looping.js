console.log ("looping dari while");
console.log ("---------------------")

let i = 0;
while (i < 10) {
  console.log (i);
    i++;
 
}


console.log ("looping dari for");
console.log ("---------------------")
for (let i= 0; i< 10; i ++) {
    console.log (i);
};


console.log ("looping dari do while");
console.log ("---------------------")
let x = 0;
do {
  x += 1;
  console.log(x);
} while (x < 5);