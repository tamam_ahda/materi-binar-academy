//segitiga atas-bawah
for (let a = 0; a < 10; a++) {
    console.log("*".repeat(a)); 
}


//segitiga dari bawah-atas
for (let b = 10; b > 0; b--) {
    console.log("*".repeat(b)); 
}

for (let i=0; i<16; i++) {
    for (let j=0; j<16; j++) {
        if ((16 - i) <= j) {
            process.stdout.write("*")
        }
        else {
            process.stdout.write(" ")
        }
    }
    process.stdout.write("\n");
};


let x= "";
for (let k= 15; k >0; k--) {
    x+= " ";
    console.log(x+"*".repeat(k));
}

