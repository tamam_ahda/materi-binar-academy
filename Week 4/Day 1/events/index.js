const Event = require("events");

//Inisiasi sebuah variabel event
const eventEmitter = new Event();

//Inisiasi function handler
function lampuMerah() {
    console.log("kendaraan anda harus berhenti");
}

function lampuHijau() {
    console.log("kendaraan anda harus jalan");
}

// Registrasi sebuah event
eventEmitter.on("lampumerah", lampuMerah)
eventEmitter.on("lampuhijau",lampuHijau )

//trigger events
eventEmitter.emit("lampumerah");
