const Event = require("events");
const { clearScreenDown } = require("readline");

//Inisiasi sebuah variabel event
const suhu = new Event();

//Inisiasi function handler
function titikDidih(number) {
  console.log(number + " merupakan titik didih");
}

function titikHangat(number) {
  console.log(number + " merupakan titik hangat");
}

function titikBeku(number) {
  console.log(number + " merupakan titk beku");
}

// Registrasi sebuah event
suhu.on("didih", titikDidih);
suhu.on("hangat", titikHangat);
suhu.on("beku", titikBeku);

for (let celcius = 200; celcius > -2; celcius--) {
  if (celcius == 100) {
    suhu.emit("didih", celcius);
  } else if (celcius == 50) {
    suhu.emit("hangat", celcius);
  } else if (celcius == 0) {
    suhu.emit("beku", celcius);
  } else {
    console.log(celcius);
  }

  // bisa pake switch juga
  //   switch (celcius) {
  //     case 100:
  //        suhu.emit("didih", celcius);
  //       break;
  //     case 50:
  //        suhu.emit("hangat", celcius);
  //       break;
  //     case 0:
  //        suhu.emit("beku", celcius);
  //       break;
  //     default:
  //       break;
  //   }
}
