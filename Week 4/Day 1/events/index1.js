const Event = require("events");

//Inisiasi sebuah variabel event
const eventEmitter = new Event();

//Inisiasi function handler
function bilanganGenap(number) {
    console.log(number + " bilangan genap");
}

function bilanganGanjil(number) {
    console.log(number + " bilangan ganjil");
}

// Registrasi sebuah event
eventEmitter.on("genap", bilanganGenap)
eventEmitter.on("ganjil",bilanganGanjil)

for (let index= 0; index <= 10; index++) {
    if (index % 2 == 0) {
        eventEmitter.emit("genap", index)
    } else {
        eventEmitter.emit("ganjil", index)
    }
}
