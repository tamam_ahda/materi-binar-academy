class Car {
    constructor() {
        this.brand = "Suzuki";

    }
    startEngine() {
        console.log("mesin menyala");
    }
}

const car = new Car();

// Modifying atau overidding
car.brand = "Honda";
car.starEngine = function() {
    console.log("methodnya aku bajak")
};

console.log(car.brand);
car.starEngine();