class Bank {
    #saldo = 1000;
    
    getSaldo() {
        console.log (`${this.#saldo}`)
    }

    #setSaldo(saldo) {
        this.#saldo += saldo;
    }
    transaction(type, amount) {
        if (type == "debit") {
            this.#saldo += amount;
        }else {
            this.#saldo -= amount;
        };
    }
}

class Atm extends Bank {
  constructor() {
      super();
  }
  withdraw(amount) {
      this.transaction("kredit", amount);
  }
  payment(amount) {
      this.transaction("kredit", amount)
  }
  deposit(amount) {
      this.transaction("debit", amount)
  }
  
  
  }

const atm = new Atm();

// saldo sebelum transaksi
atm.getSaldo();

// Bayar listrik
atm.payment(500); //500

// ambil uang
atm.withdraw(200); //300

//setot tunai 
atm.deposit(1000) // 1300

// saldo sesudah transaksi
atm.getSaldo()
