class Bank {
    #saldo = 1000;
    
    getSaldo() {
        console.log (`${this.#saldo}`)
    }

    #setSaldo(saldo) {
        this.#saldo += saldo;
    }
    transaction(type, amount) {
        if (type == "debit") {
            this.#saldo += amount;
        }else {
            this.#saldo -= amount;
        };
    }
}

class Atm extends Bank {
  constructor() {
      super();
  }
  withdraw(amount) {
      this.transaction("kredit", amount);
  }
  payment(amount) {
      this.transaction("kredit", amount)
  }
  deposit(amount) {
      this.transaction("debit", amount)
  }
  
  
  }

  // protect object with Object.freeze
const atm = Object.freeze(new Atm());


// overidding function
atm.getSaldo = function () {
    console.log(50000)
}

atm.getSaldo();