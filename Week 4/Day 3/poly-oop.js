// super class
class Engine {
    startEngine() {
      console.log(`mesin ${this.type} menyala`);
    }
    stopEngine() {}
  }
  
  // child class
  class Car extends Engine {
    horn() {} // one function can be used on many class (polymorphism)
  }
  
  // Child Class / sub class
  class Toyota extends Car {
    constructor(type) {
      super();
      this.type = type;
    }
  }
  
  class Honda extends Car {
    constructor(type) {
      super();
      this.type = type;
    }
  }
  
  class Daihatsu extends Car {
    constructor(type) {
      super();
      this.type = type;
    }
  }
  
  const avanza = new Toyota("Avanza");
  const mobilio = new Honda("Mobilio");
  const xpander = new Daihatsu("X-pander");
  
  avanza.startEngine();
  mobilio.startEngine();
  xpander.startEngine();
  