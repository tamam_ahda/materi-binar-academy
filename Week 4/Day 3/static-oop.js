class Car {
  static brand2 = "Honda";
  constructor() {
    // Instance Property
    this.brand = "Toyota";
    this.type = "Honda";
  }
  // Instance Method
  starEngine() {
    console.log("mobil menyalakan mesin");
  }
  stopEngine() {}

  // static method
  static getBrand2() {
    return Car.brand2;
  }
}

const car = new Car();
car.starEngine()

// Jika ingin mengambil static property
console.log(Car.brand2);
console.log(Car.getBrand2());