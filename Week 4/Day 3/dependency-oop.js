class Car {
    checkWheel() {
        console.log ("ada 4 roda");
    }
}

class Motor {
    checkWheel() {
        console.log ("ada 2 roda");
    }
}

class Engine {
    checkEngine() {
        console.log ("engine is fine");
    }
}

class Honda {
    constructor(type, car, motor, engine) {
        this.type = type;
        this.car = dependency.car;
        this.motor = dependency.motor;
        this.engine = dependency.engine;
    }
}

const car= new Car();
const motor = new Motor();
const engine = new Engine();

const dependency = {car, motor, engine};

const mobilio = new Honda("mobilio", dependency);

mobilio.car.checkWheel();
mobilio.motor.checkWheel();
mobilio.engine.checkEngine();