class Fan {
    constructor (merk, warna, berat, tipe, putaran) {
        this.name = merk;
        this.color = warna;
        this.weight = berat;
        this.type = tipe;
        this.rpm = putaran;
    }
    start () {
        console.log(`kipas angin ${this.name}, warnanya ${this.color}, beratnya ${this.weight}, dan tipenya ${this.type} sudah menyala dengan putaran ${this.rpm} rpm`);
    }
    
    stop () {
        console.log(`kipas angin ${this.name}, warnanya ${this.color}, beratnya ${this.weight}, dan tipenya ${this.type} sudah mati`) 
    }
    changeType(tipe) {
        this.type = tipe;
    }
    changeRpm (putaran) {
        this.rpm = putaran;
    }
}

const fan = new Fan("Cosmos Wadesta", "putih", 700, "berdiri", "2000");

fan.start();
fan.stop()

fan.changeType ("nempel di dinding");
fan.changeRpm (2500);

fan.start ();
fan.stop ();