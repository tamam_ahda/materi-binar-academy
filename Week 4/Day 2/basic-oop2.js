class Mobil {
    constructor(name, color) {
        // Inisiasi property dalam object class
        this.name= name;
        this.weight= 800;
        this.model= "MPV";
        this.color= color;
    }

    start(){
        console.log(`${this.name} yang warnanya ${this.color} telah menyala`);
    }
    drive() {
        console.log(`${this.name} siap digunakan`);
    }
    brake() {
        console.log(`${this["name"]} telah di rem`);
    }
    stop () {
        console.log(`${this["name"]} telah berhenti`);
    }
    changeColor(color) {
        this.color= color;
    }
}


// Create new instance
const mobil = new Mobil("Honda Jazz", "Merah");
const mobil2 = new Mobil("Suzuki Ertiga", "Hijau");

mobil.start();
mobil2.start();

mobil.changeColor ("biru");
mobil.start();