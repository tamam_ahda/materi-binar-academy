const mobil = {
  name: "Toyota Avanza",
  weight: 800,
  model: "MPV",
  color: "Putih",
  start: function () {
    console.log(`${this.name} telah menyala`);
  },
  drive: function () {
    console.log(`${this.name} siap digunakan`);
  },
  brake: function () {
    console.log(`${this["name"]} telah di rem`); // cara lain panggil name
  },
  stop: function () {
    console.log(`${this["name"]} telah berhenti`); // cara lain panggil name
  },
};

// Memanggil Prperty
console.log(mobil.name);
console.log(mobil["model"]);
console.log(mobil["color"]);

// Memanggil Method
mobil.start(); // atau bisa dengan: mobil["start"]()
mobil.drive();
mobil.brake();
mobil.stop();

// Ganti nilai property
mobil.name = "Honda Jazz";
mobil.start();