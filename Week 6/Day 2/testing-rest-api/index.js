const express = require("express");

const bodyParser = require("body-parser");

const db = require("./models");

const app = express();

app.use(bodyParser.json());

app.post("/foods", async (req, res) => {
  const name = req.body["name"];
  const price = req.body["price"];
  const description = req.body["description"];
  const calories = req.body["calories"];
  const data = await db.foods.create({
    name: name,
    price: price,
    description: description,
    calories: calories,
    createdAt: new Date(),
    updateAt: new Date(),
  });

  res.json({ messages: "success insert data foods", data: data });
});

app.get("/foods", async (req, res) => {
  const data = await db.foods.findAll();

  res.json({ message: "success read data foods from database", data: data });
});

app.put("/foods", async (req, res) => {
  const name = req.body["name"];
  const price = req.body["price"];
  const description = req.body["description"];
  const calories = req.body["calories"];
  const data = await db.foods.update(
    {
      name: name,
    },
    { price: price },
    { description: description },
    { calories: calories }
  );

  res.json({ messages: "success update foods from database", data: data });
});

app.delete("/foods", async (req, res) => {
  const id = req.query.id;
  await db.foods.destroy({
    where: { id: id },
  });
  res.json({ messages: "seucces delete foods forom database" });
});

app.listen(3000, () => console.log("running on port 3000"));
