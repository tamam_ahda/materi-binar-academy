const express = require("express");

const app = express();
const db = require("./models");
const bodyParser = require("body-parser");
app.use(bodyParser.json());

app.get("/testing", (req, res) => {
  res.json({ messages: "anda telah terdaftar dalam situs kami" });
});

app.get("*", (req, res) => {
  res.status(404).json({ message: "routing tidak ditemukan" });
});

app.listen(3200, (req, res) => console.log("running on port 3200"));
