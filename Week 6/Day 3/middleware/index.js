const express = require("express");

const app = express();
const db = require("./models");
const bodyParser = require("body-parser");
app.use(bodyParser.json());

const checkRequestField = (req, res, next) => {
  if (req.query.username && req.query.password) {
    next();
  } else {
    return res.send("data username dan password tidak ada");
  }
};

const checkRequestField2 = (req, res, next) => {
  if (
    req.body.username &&
    req.body.password &&
    req.body.age &&
    req.body.email
  ) {
    next();
  } else {
    return res.send("data tidak lengkap");
  }
};

const checkRequestField3 = (req, res, next) => {
  if (req.body.username && req.body.id) {
    next();
  } else {
    return res.send("belum memasukkan username & id");
  }
};

const checkDataTypes = async (req, res, next) => {
  const parseUsarname = (await Number.isNaN(parseInt(req.query.username)))
    ? ""
    : parseInt(req.query.username);
  if (typeof parseUsarname === "string" && req.query.password === "string") {
    next();
  } else {
    return res.send("tidak sesai data type");
  }
};

const checkDataTypesAge = async (req, res, next) => {
  if (typeof req.body.age === "string") {
    return res.send("age tidak sesuai data type");
  } else {
    next();
  }
};

const checkLogin = async (req, res, next) => {
  const dataCheck = await db.users.findOne({
    where: { username: req.query.username, password: req.query.password },
  });

  if (dataCheck) {
    next();
  } else {
    return res.send("kamu tidak terdaftar di table users");
  }
};

app.get("/", checkRequestField, checkDataTypes, checkLogin, (req, res) =>
  res.send("kamu telah terdaftar di data base")
);

app.get("*", (req, res) => {
  res.status(404).json({ message: "routing tidak ditemukan" });
});

app.post("/", checkRequestField2, checkDataTypesAge, async (req, res) => {
  const username = req.body["username"];
  const password = req.body["password"];
  const age = req.body["age"];
  const email = req.body["email"];
  const dataBaru = await db.users.create({
    username: username,
    password: password,
    age: age,
    email: email,
    createdAt: new Date(),
    updateAt: new Date(),
  });
  res.json({ messages: "selamat, kamu telah bergabung", data: dataBaru });
});

app.post("*", (req, res) => {
  res.status(404).json({ message: "routing tidak ditemukan" });
});

app.put("/", checkRequestField3 ,async (req, res) => {
  const username = req.body["username"];
  const id = req.body.id;
  await db.users.update(
    {
      username: username,
    },
    { where: { id: id } }
  );
  res.json({
    message: `success update username ${username} with new id ${id}`,
  });
});

app.put("*", (req, res) => {
  res.status(404).json({ message: "routing tidak ditemukan" });
});

app.delete("/", async (req, res) => {
  const id = req.query.id;
  await db.users.destroy({ where: { id: id } });
  res.send("success delete from users with id " + id);
})

app.delete("*", (req, res) => {
  res.status(404).json({ message: "routing tidak ditemukan" });
});

app.listen(4000, () => console.log("running in port 4000"));
